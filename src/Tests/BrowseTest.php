<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1-7-19
 * Time: 15:40
 */

namespace App\Tests;

use Symfony\Component\Panther\DomCrawler\Link;

class BrowseTest extends Base
{
    private $crawler;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->crawler = $this->client->request('GET', '/');
    }

    public function test()
    {
        //get categories
        $categoryLinks = array_map(function (Link $link) {
            return $link->getUri();
        }, $this->crawler->filter('.dropdown-menu a')
            ->links());

        foreach ($categoryLinks as $categoryLink) {
            //open category page
            $this->crawler = $this->client->request('GET', $categoryLink);

            //determine amount of pages
            $paginationCount = $this->crawler->filter('.pagination a')->count();
            if ($paginationCount === 1) $paginationCount = 2;

            //loop over every page
            for ($i = 1; $i < $paginationCount; $i++) {
                //get articles
                $articleLinks = array_map(function (Link $link) {
                    return $link->getUri();
                }, $this->crawler->filter('.article-grid .row .article-image a')
                    ->links());

                foreach ($articleLinks as $articleLink) {
                    //visit each
                    $this->client->request('GET', $articleLink);
                }

                //go to next page
                if ($paginationCount !== 2)
                {
                    $this->crawler = $this->client->request('GET', $categoryLink . '?page='.($i+1));
                }
            }
        }
    }
}