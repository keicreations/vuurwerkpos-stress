<?php

namespace App\Tests;


use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\TimeOutException;
use Faker\Factory;
use Symfony\Component\Panther\PantherTestCase;

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1-7-19
 * Time: 11:26
 */
class OrderTest extends Base
{
    public function test()
    {
        //init
        $crawler = $this->client->request('GET', '/');
        //product selection
        $this->client->click($crawler->selectLink('Silver Thunder')
            ->link());
        //add to cart
        $this->clickQuerySelector('.cart-add');
        //complete order
        $this->clickQuerySelector('.btn-complete');
        //change amount to random number and trigger event
        $this->client->waitFor('.cart-row-price');
        $amount = rand(1, 5);
        $this->client->executeScript("document.querySelector('.cart-row-price .form-control [value=\"$amount\"]').selected=true;document.querySelector('.cart-row-price .form-control').dispatchEvent(new Event('change')) ");
        //checkout
        $this->clickQuerySelector('.cart .btn-primary');
        //continue without account
        $this->clickQuerySelector('.btn-secondary');
        //fill form
        $faker = Factory::create('nl_NL');
        $this->setValue('#customer_account_firstName', $faker->firstName);
        $this->setValue('#customer_account_lastName', $faker->lastName);
        $this->setValue('#customer_account_address', $faker->streetAddress);
        $this->setValue('#customer_account_zipcode', $faker->postcode);
        $this->setValue('#customer_account_city', $faker->city);
        $this->setValue('#customer_account_phonenumber', $faker->phoneNumber);
        $this->setValue('#customer_account_email', preg_replace('/@example\..*/', '@keicreations.nl',$faker->safeEmail));
        $this->clickQuerySelector('label[for=\'customer_account_createAccount\']');
        $password = $faker->password;
        $this->setValue('#customer_account_newPassword_first', $password);
        $this->setValue('#customer_account_newPassword_second', $password);
        //place order
        $this->clickQuerySelector('#customer_account_placeOrder');
        //select pickup window
        $this->clickQuerySelector('label[for=\'check_out_pickupTimeslot_'.rand(0, 38).'\']');
        //payment method
        $this->clickQuerySelector('label[for=\'check_out_paymentMethod_3\']');
        //agree
        $this->clickQuerySelector('label[for=\'check_out_agreeToTermsAndConditions\']');
        //finalize order
        $this->clickQuerySelector('#check_out_placeOrder');
        //giropay
        $this->clickQuerySelector('.paymentMethods a');
        //token
        $this->setValue('#token-input', '');
        //pay
        $this->clickQuerySelector('.simulationMethod');
    }
}