<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1-7-19
 * Time: 15:35
 */

namespace App\Tests;

use Symfony\Component\Panther\Client as PantherClient;
use Symfony\Component\Panther\PantherTestCase;
use Symfony\Component\Panther\Client;

class Base extends PantherTestCase
{
    protected $client;

    protected static function createPantherClient(array $options = [], array $kernelOptions = []): PantherClient
    {
        self::startWebServer($options);
        if (null === self::$pantherClient) {
            self::$pantherClient = Client::createChromeClient(null, null, $options, self::$baseUri);
        }

        if (\is_a(self::class, KernelTestCase::class, true)) {
            static::bootKernel($kernelOptions);
        }

        return self::$pantherClient;
    }

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
//        dd($this->determinePort());
        //dump($this->determinePort());
        parent::__construct($name, $data, $dataName);
        $this->client = static::createPantherClient([
            'external_base_uri' => 'https://webshop-demo.vuurwerkpos.kei.io',
            'port' => $this->determinePort()
        ]);
    }

    private function determinePort(): int
    {
        $port = 9516;
        $found = false;
        while (!$found) {
            if (!is_resource(@fsockopen('localhost', $port))) {
                $found = true;
            } else {
                $port++;
            }
        }
        return $port;
    }

    protected function setValue($qs, $val)
    {
        $this->client->executeScript('document.querySelector("'.$qs.'").value="'.$val.'"');
    }

    protected function clickQuerySelector($qs)
    {
        $this->client->waitFor($qs);
        $this->client->executeScript('document.querySelector("'.$qs.'").click()');
    }
}